<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @include('layout._metas')

    <link rel="shortcut icon" href="{{ asset('assets_front/img/favicon.ico')}}?ver=19.0">
    <link rel="stylesheet" href="{{asset('assets_front/css/app.css')}}?ver=18.0">
    <link rel="stylesheet" href="{{asset('assets_front/webfont/AgustineScript/stylesheet.css')}}?ver=18.0">
    <link rel="stylesheet" href="{{asset('assets_front/icons_font/css/style.css')}}?ver=18.0">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css" /> --}}
    {{-- <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> --}}
    <link rel="stylesheet" href="{{asset('assets_front/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('assets_front/css/animate.css')}}">
    <script src="{{asset('assets_front/js/lazysizes.js')}}"></script>

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /> --}}

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177593932-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-177593932-1');
        </script>

    @toastr_css
</head>

<body id="scrollbar-mom">
    
    
    <div id="loader" class=" text-center">
        <div id="loaderlofts_hype_container" class="HYPE_document" style="margin:auto;margin-top: 40vh;position:relative;width:300px;height:200px;overflow:hidden;">
            <script type="text/javascript" charset="utf-8" src="{{asset('assets_front/loader/loaderlofts_hype_generated_script.js?79713')}}"></script>
        </div>
    </div>
    {{-- <img  src="{{url('assets_front/img/cond-logo-full.png')}}" width="380px" height="auto" class=" text-center" alt=""> --}}
    <nav class="navbar montelier-menu navbar-expand-lg fixed-top navbar-dark bg-dark collapse fade justify-content-between py-0"
        id="navbarNav">
        <a class="navbar-brand m-0" href="{{ route('index') }}">
            <img src="{{asset('assets_front/img/condominio-montelier-logo.svg')}}" class="py-32 d-none d-md-block"
                width="auto" height="180px" alt="" loading="lazy">
            <img src="{{asset('assets_front/img/condominio-montelier-logo.svg')}}" class="py-32 d-md-none"
                width="60%" height="auto" alt="" loading="lazy">
        </a>
        @yield('navigation') 

    </nav>
    <button id="nav-toggler" data-aos="fade-down" class="btn btn-dark btn-top-menu" type="button"
        data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
        aria-label="Toggle navigation">
        {{-- <i class="icon icon-bars"></i> --}}
        <div id="nav-icon1">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </button>
    <main id="main">
        @yield('content')
    </main>


    @stack('modais')

    @jquery
    <!-- Bootstrap JS -->
    {{-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('assets_front/js/popper.min.js')}}"></script>
    
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('assets_front/js/bootstrap.min.js')}}"></script>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    {{-- <script src="{{asset('assets_front/js/ionicons.js')}}"></script> --}}

    <script src="{{asset('assets_front/js/directive.js')}}"></script>
    <script src="{{asset('assets_front/js/jquery.mousewheel.js')}}"></script>
    <script src="{{asset('assets_front/js/knockout.js')}}"></script>
    <script src="{{asset('assets_front/js/Stellar.js')}}"></script>
    <script src="{{asset('assets_front/js/parallax.min.js')}}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script> --}}
    <script src="{{asset('assets_front/js/SmoothScroll.js')}}"></script>
    {{-- <script src="{{asset('assets_front/js/cmomentumScroll.js')}}"></script> --}}
    {{-- <script src="{{asset('assets_front/js/smooth-scrollbar.js')}}"></script>
    <script>
            var Scrollbar = window.Scrollbar;
            Scrollbar.init(document.querySelector('#scrollbar-mom'));
    </script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script> --}}
    <script src="{{asset('assets_front/js/scrollmagic.min.js')}}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168658682-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-168658682-1');
    </script>
    {{-- <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script> --}}
    
    <script>
        function collapsingList(evt) {
            if(( window.innerWidth < 765 )){
                window.dispatchEvent(new Event('resize'));
    
            }
        }


        var c_btn = document.getElementsByClassName("c-btn-list");
        var c_i;
        for (c_i = 0; c_i < c_btn.length; c_i++) {
            c_btn[c_i].addEventListener(
                'click', collapsingList, false
            );
        }

        $('.c-btn-list').on('click',function(e){
            if($(this).parents('.row').children('.list-data').children('.collapse').hasClass('show')){
                e.preventDefault();
                e.stopPropagation();
            }

        });

    </script>

    <script>
        var smoothRef;
        $(window).bind("load", function() {
            var loader = document.getElementById("loader");
            loader.classList.add("animate__animated");
            loader.classList.add("animate__fadeOut");

            loader.addEventListener('animationend', () => {
                loader.classList.add("d-none");
            });


            if(( window.innerWidth >= 765 )){
                const params = {
                    duration: 800,
                    timingFunction: 'cubic-bezier(0.23, 1, 0.32, 1)'
                }
                
                if(( window.innerWidth >= 765 )){
                    smoothRef = new SmoothScroll('.allContent', params)
                }
            }

        });

        $('#nav-toggler').click(function(){
            $("#nav-icon1").toggleClass('open');
        });

        function collapseMenu(collapseId) {
            $(collapseId).collapse('toggle');   
            $("#nav-icon1").toggleClass('open');         
        }
        function collapseMenuHome(collapseId, moveToId) {
            event.preventDefault();
            $(collapseId).collapse('toggle');   
            $("#nav-icon1").toggleClass('open');         
            smoothRef._handleLink(moveToId);
        }
    </script>
    @stack('scripts')

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    {{-- <script>
            console.log("carousel being stoped");
            $('.carousel').each(function () {  
                $(this).carousel('pause');
            })
    </script> --}}

    @toastr_js
    @toastr_render


</body>

</html>