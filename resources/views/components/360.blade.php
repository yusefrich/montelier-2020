<div  class="sidemenu_360">
    <ul class="pl-0 text-light d-none d-md-block">
        <li data-aos="fade-down">
            <img src="{{asset('assets_front/img/condominio-montelier-logo.svg')}}" class="py-32" width="auto" height="180px" alt="" loading="lazy">
        </li>
    </ul>
    <ul class="pl-0 text-light d-md-none pt-1">
        <li data-aos="fade-down">
            <img src="{{asset('assets_front/img/condominio-montelier-logo.svg')}}" class="py-32 pl-3 " width="60%" height="auto" alt="" loading="lazy">
        </li>
    </ul>
</div>
<div data-aos="fade-up" class="container-large hero-content hero-content-first mx-auto">
    <h1 data-aos="fade-right" data-aos-delay="150" class="text-uppercase text-light font-weight-200 hero-header-text">
        Agora você tem o lugar perfeito
    </h1>
    <h1 data-aos="fade-right" data-aos-delay="300" class="mont-cursive">
        na serra
    </h1>
    <button data-aos="fade-right" data-aos-delay="450" data-toggle="modal" onclick="SetTheVideo('https://www.youtube.com/embed/X9jPidLc_44?autoplay=1')" data-target="#video" class="btn btn-outline-light"><span style="position: relative; top: -5px">VEJA O VÍDEO</span> <ion-icon style="font-size: 25px" name="play-outline"></ion-icon>
    </button>
</div>
<div class="overflow-hidden">
    <div id="exp360" class="experiencia360 ">

        <div class="bg-fade"></div>
        <div id="scrollable" class="img-hero-scroll">
            <img src="{{url('assets_front/img/main-hero-bg.jpg')}}" class="position-relative" id="bg-hero" alt="">
        </div>
        {{-- <div style="width: 100%; height: 100%; background-image: url('{{url('assets_front/img/hero-covert.png')}}');" id="bg-hero"></div> --}}
        {{-- <iframe class="iframe-hide" style="border:none;" width="100%" height="100%" id="iframe360" allowvr="yes" frameborder="0" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen></iframe> --}}
    </div>
</div>
@push('modais')
<div class="modal fade pr-0" id="video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-full">
        <div class="modal-content bg-transparent">
            <div class="modal-body px-0">
                <div class="d-flex justify-content-end">
                    <button onclick="SetTheVideo('')" style="z-index: 10" type="button" class="btn btn-light btn-round p-3 mr-4" data-dismiss="modal" aria-label="Close">
                        <i class="icon icon-cross"></i>
                    </button>
                </div>
                <div style="width: 100vw" id="carousel_img_obras" class="carousel position-absolute slide vertical-center" data-ride="carousel">
                    <div  class="carousel-inner overflow-visible">
                        <div  class="carousel-item px-200 img-carousel-lib active">
                            <iframe id="video_frame" width="100%" height="660px" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            {{-- <div class="carousel-caption d-none d-md-block img-desc">
                                <h4 data-bind="html: title" class="text-light lib-desc-offset text-uppercase"></h4>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer border-0">
            </div> --}}
        </div>
    </div>
</div>
@endpush

{{-- @push('modais')
<div class="modal fade " id="modal_tutorial_360" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog vertical-center-modal">
      <div class="modal-content bg-light border-radius-0">
        <div class="modal-body text-center p-32 py-4  border-radius-0">
            <div class="row mx-0 px-3 px-md-0  mb-32">
                <div class="col-md-6 ">
                    <img class="mb-3" src="{{asset('assets_front/img/tuto_360_1.svg')}}" alt="">
                    <p class="text-dark">Clique na foto e arraste para visualizar o ambiente em 360°</p>
                </div>
                <div class="col-md-6">
                    <img class="mb-3" src="{{asset('assets_front/img/tuto_360_2.svg')}}" alt="">
                    <p class="text-dark">Clique na foto e arraste para visualizar o ambiente em 360°</p>
                </div>
            </div>
            <button type="button" class="btn btn-outline-dark text-uppercase" data-dismiss="modal" aria-label="Close">
                Entendi
            </button>
        </div>
      </div>
    </div>
  </div>
@endpush
 --}}
@push('scripts')
    <script>

        function SetTheVideo(videoUrl) {
            document.getElementById('video_frame').src = videoUrl;
        }

        window.addEventListener('scroll', function(e) {
            /* var elmnt = document.getElementById("scrollable"); */
            var x = window.scrollY;

            /* var newelmnt = document.getElementById("scrollable");
            newelmnt.scrollLeft = x; */
            /* $("#bg-hero").animate({ "right": (x)+"px" }, 0) */

            $("#bg-hero").css("transform", "translateX(-"+(x/2)+"px)")
        });

    </script>
@endpush

