<div id="sobre" class="container-large mx-auto position-relative">
    <div  id="card-hero" class="mont-box ml-auto">
        <h3 >MONTELIER</h3>
        <h4 >PARA GRANDES MOMENTOS</h4>
        <p >Sua vida merece a tranquilidade da natureza, a beleza da serra e o luxo do Montelier, o seu Condomínio de
            Campo. Inspirado no charme da cidade francesa de Montélier e construído em uma das serras da região de
            Canafístula, bem no meio do brejo paraibano, o condomínio é o lugar perfeito para grandes momentos.</p>
        <p >O projeto arquitetônico atemporal, assinado por renomados arquitetos, traz ao seu condomínio de campo um
            design contemporâneo e minimalista, com toda sofisticação e conforto.</p>
    </div>
    <div class="overflow-hidden">
        <img  class="img-fluid w-100"  src="{{url('assets_front/img/img-sec-1-n.jpg')}}"  alt="...">{{-- hero-lot-cover.jpg --}}
    </div>

</div>

@push('scripts')
<script>
    window.addEventListener('scroll', function(e) {
        if(( window.innerWidth >= 765 )){
            var x = window.scrollY;
            var frac = x/5
            x = x - frac;
            if(x < 1200){
                
                $("#card-hero").css("transform", "translateY(-"+((x) /4)+"px)")
            }
        }
    });



</script>
@endpush