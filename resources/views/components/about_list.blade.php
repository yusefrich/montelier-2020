<div class="container-large mx-auto list-spacing" id="about-list">
    <div data-aos="fade-right" class="mont-collapse " id="accordion">
        <div class="row ">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left" data-toggle="collapse" data-target="#c-guarita"
                    aria-expanded="true" aria-controls="c-guarita" >
                    Guarita
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-guarita" class="animate__animated animate__fadeIn collapse show mr-md-3" aria-labelledby="h-guarita" data-parent="#accordion">
                    <img style="object-fit: cover;" data-aos="fade-right" data-aos-delay="250" data-sizes="auto" class="img-fluid lazyload" data-srcset="{{asset('assets_front/img/g1/guarita_2.jpeg')}}" data-src="{{asset('assets_front/img/g1/guarita_2.jpeg')}}" alt="">

                    <div  class="mont-box ">
                            <h4 >VOCÊ CHEGOU AO PARAÍSO</h4>
                            <p >A arquitetura se une à natureza no Montelier e proporciona uma experiência única que se inicia logo na chegada, a começar pela guarita suntuosa e imponente, marcada por sua estrutura monumental. </p>
                            <p >Somos a revolução do conceito de condomínio de campo.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Playground"
                    aria-expanded="true" aria-controls="c-Playground" >
                    Playground
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-Playground" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Playground" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/2-Playground-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/2-Playground-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>Para valorizar a infância</h4>
                        <p>Aqui acreditamos que o universo infantil precisa ser de muita diversão. Nela a criança interage, aprende a lidar com o mundo, forma sua personalidade e cria um mundo de aventura.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-kids"
                    aria-expanded="true" aria-controls="c-kids" >
                    Espaço Kids
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-kids" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-kids" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/EspacoKids-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/EspacoKids-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>O universo todo em um único lugar</h4>
                        <p>As memórias da infância marcam toda uma vida. Com os espaços pensados exclusivamente para os pequenos, ser criança no Montelier tem um gosto especial. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-ca"
                    aria-expanded="true" aria-controls="c-ca" >
                    CASA NA ÁRVORE
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-ca" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-ca" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/CasaNaArvore-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/CasaNaArvore-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>UM SONHO SEM LIMITES PARA IMAGINAÇÃO</h4>
                        <p>Quando o assunto é sonho, a imaginação vai longe, especialmente a das crianças. Aqui, a criançada brinca, sonha e não tem limites para sua diversão. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-festas"
                    aria-expanded="true" aria-controls="c-festas" >
                    Salão de Festas
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-festas" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-festas" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/4-SalaoDeFestas-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/4-SalaoDeFestas-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>LUXO E CELEBRAÇÃO</h4>
                        <p>A vida é repleta de bons momentos e no Montelier você terá mais motivos para comemorar em nossos espaços luxuosos, como o deslumbrante salão de festas.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Academia"
                    aria-expanded="true" aria-controls="c-Academia" >
                    Academia
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-Academia" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Academia" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/5-_Academia-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/5-_Academia-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>ACONCHEGO PARA CORPO E MENTE</h4>
                        <p>Se exercitar e manter a boa forma nunca será tão prazeroso quanto em nossa academia. Mas para quem prefere a diversão entre amigos, o Montelier conta com quadra de tênis e quadra poliesportiva. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-cooper"
                    aria-expanded="true" aria-controls="c-cooper" >
                    Pista de cooper
                </button>
            </div>
            <div  class="col-md-7 list-data">
                <div style="right: 0" id="c-cooper" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-cooper" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/6-PistaDeCooper-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/6-PistaDeCooper-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>SENSAÇÃO DE IR SEMPRE MAIS ALÉM</h4>
                        <p>O cooper também está garantido para os amantes da corrida ao ar livre.</p>
                        <p>Não podemos esquecer da vista de impressionar e o ar puro que revigora e energiza. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Quadra"
                    aria-expanded="true" aria-controls="c-Quadra" >
                    Quadra Poliesportiva
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-Quadra" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Quadra" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/7-QuadraPoliesportiva-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/7-QuadraPoliesportiva-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>UM AMBIENTE PERFEITO DE ESPORTE E LAZER</h4>
                        <p>Se o que você quer é praticar várias modalidades esportivas ou apenas relaxar com os amigos nas suas férias, nossa quadra poliesportiva é uma ótima alternativa.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Piscina"
                    aria-expanded="true" aria-controls="c-Piscina" >
                    Piscina
                </button>
            </div>
            <div  class="col-md-7 list-data">
                <div style="right: 0" id="c-Piscina" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Piscina" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/8-Piscina-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/8-Piscina-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>LAZER COMPLETO</h4>
                        <p>Com a piscina de borda infinita, cascata e a vista magnífica para a serra. Ao lado da família e amigos, o tempo vai passar devagar na área de camping e apoio da piscina.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Haras"
                    aria-expanded="true" aria-controls="c-Haras" >
                    Haras
                </button>
            </div>
            <div  class="col-md-7 list-data">
                <div style="right: 0" id="c-Haras" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Haras" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/9-Haras-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/9-Haras-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>CONTATO COM ANIMAIS E COM A NATUREZA</h4>
                        <p>Os apaixonados por cavalos podem desfrutar de um espaçoso haras e
                            aproveitar as inúmeras trilhas da região para cavalgar. Pronto para o passeio?</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-Gourmet"
                    aria-expanded="true" aria-controls="c-Gourmet" >
                    Espaço Gourmet
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-Gourmet" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-Gourmet" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/10-EspacoGourmet-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/10-EspacoGourmet-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>FESTEJE CONTEMPLANDO UMA VISTA SEM IGUAL</h4>
                        <p>Se a ideia é desfrutar de boas companhias e boa comida em um cenário exclusivo, o espaço gourmet está pronto para você.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse" data-target="#c-reda"
                    aria-expanded="true" aria-controls="c-reda" >
                    Redário
                </button>
            </div>
            <div class="col-md-7 list-data">
                <div style="right: 0" id="c-reda" class="animate__animated animate__fadeIn collapse mr-md-3" aria-labelledby="h-reda" data-parent="#accordion">
                    <img style="object-fit: cover;" class="img-fluid lazyload" data-sizes="auto" data-src="{{asset('assets_front/img/g1/11-Redario-min.jpg')}}" data-srcset="{{asset('assets_front/img/g1/11-Redario-min.jpg')}}" alt="">
                    <div class="mont-box ">
                        <h4>UMA VISTA APAIXONANTE</h4>
                        <p>Se a ideia é relaxar, descansar ou contemplar um belo dia no campo, o redário do Montelier é o ambiente perfeito.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
