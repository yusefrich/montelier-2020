<div class="bg-dark-green">
    <div id="regiao-about" class="container-large mx-auto  position-relative bg-dark-green">
        <div data-aos="fade-right" id="regiao-about-card" class="mont-box mont-box-green ml-auto">
            <h3  class="text-uppercase mb-32">O paraíso fica logo ali</h3>
            <p >O Montelier está localizado em Bananeiras, região de natureza em abundância e clima aconchegante.
                Você pode chegar ao condomínio por três cidades: Bananeiras, Borborema e Solânea. </p>
            <p >Não importa o
                caminho escolhido, a experiência será única pela beleza natural e pelos atrativos turísticos de
                alguns dos roteiros mais tradicionais do estado.</p>
        </div>
        <img class="img-fluid w-100" src="{{url('assets_front/img/img-sec-2.jpg')}}" alt="">
    </div>
</div>

@push('scripts')
<script>
    window.addEventListener('scroll', function(e) {
        var cardElm = document.getElementById("regiao-about-card");

        var distanceFromTop = cardElm.getBoundingClientRect().top; 
        var distanceOnBottom = (screen.height/2) - distanceFromTop;

        if(( window.innerWidth >= 765 )){
            var x = (window.scrollY) - ($('#regiao-about-card').offset().top - 700) ;
            
            if(x < 1000){
                $("#regiao-about-card").css("transform", "translateY(-"+((x) /4)+"px)")
            }
        }
    });



</script>
@endpush