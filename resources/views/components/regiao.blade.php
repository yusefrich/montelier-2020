<div data-aos="fade-up" id="regiaosec" class="container-large hero-content-sm mx-auto">
    <h1 data-aos="fade-right" data-aos-delay="250" class="text-uppercase text-light font-weight-200 hero-header-text">
        DESCUBRA O PRAZER DE VIVER E O AMOR PELA VIDA
    </h1>
    <h1 data-aos="fade-right" data-aos-delay="400" class="mont-cursive">
        na serra
    </h1>
</div>
<div class="overflow-hidden">
    <div id="hero-regiao" class="hero-content-header position-relative">

        <div class="bg-fade"></div>
        <div id="scrollable-2" class="img-hero-scroll">
            <img data-sizes="auto" data-src="{{url('assets_front/img/hero-regiao-cover.jpg')}}" data-srcset="{{url('assets_front/img/hero-regiao-cover.jpg')}}"  style="height: 100%" class="position-relative lazyload" id="bg-regiao-hero" alt="">
        </div>
        {{-- <div style="width: 100%; height: 100%; background-image: url('{{url('assets_front/img/hero-covert.png')}}');" id="bg-hero"></div> --}}
        {{-- <iframe class="iframe-hide" style="border:none;" width="100%" height="100%" id="iframe360" allowvr="yes" frameborder="0" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen></iframe> --}}
    </div>
</div>


@push('scripts')
    <script>

        window.addEventListener('scroll', function(e) {
            var regelmnt = document.getElementById("scrollable-2");
            var regImgelmnt = document.getElementById("bg-regiao-hero");

            var x = window.scrollY;
            var distanceFromTop = regelmnt.getBoundingClientRect().top; 

            var distanceOnBottom = (screen.height/1.5) - distanceFromTop;

            if( distanceOnBottom > 0 ){
                distanceOnBottom = distanceOnBottom/2
                if(distanceOnBottom > (regImgelmnt.width - window.innerWidth)){
                    distanceOnBottom = regImgelmnt.width - window.innerWidth;
                }
                $("#bg-regiao-hero").css("transform", "translateX(-"+(distanceOnBottom)+"px)")

            }else {
                $("#bg-regiao-hero").css("transform", "translateX(-"+(0)+"px)")
            }


            /* var newelmnt = document.getElementById("scrollable");
            newelmnt.scrollLeft = x; */
            /* $("#bg-hero").animate({ "right": (x)+"px" }, 0) */


        });

    </script>
@endpush

