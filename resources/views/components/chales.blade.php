<div data-aos="fade-up" id="chales-section" class="container-large hero-content-sm mx-auto">
    <h1 data-aos="fade-right" data-aos-delay="300" class="text-uppercase text-light font-weight-200 hero-header-text">
        CHALÉS 
    </h1>
    <h1 style="max-width: 700px;"  data-aos="fade-right" data-aos-delay="600" class="mont-cursive">
        para grandes momentos
    </h1>
    <a data-aos="fade-right" data-aos-delay="450"  href="https://chales.condominiomontelier.com.br" target="_blank" class="btn btn-outline-light heroBtnChales">
        VISITE O SITE 
        <img style="position: relative; top: -2px"class="pl-1 normalImg" src="{{url('assets_front/img/arrow.svg')}}" alt="">
        <img style="position: relative; top: -2px"class="pl-1 hoverImg" src="{{url('assets_front/img/arrow-dark.svg')}}" alt="">
    </a>
</div>
<div  class="overflow-hidden">
    <div id="hero-chales" class="hero-content-header position-relative">

        <div class="bg-fade"></div>
        <div id="scrollable-4" class="img-hero-scroll">
            <img data-sizes="auto" data-src="{{url('assets_front/img/hero-chales-cover-n.jpg')}}" data-srcset="{{url('assets_front/img/hero-chales-cover-n.jpg')}}" style="height: 100%" class="position-relative lazyload" id="bg-chales-hero" alt="">
        </div>
        {{-- <div style="width: 100%; height: 100%; background-image: url('{{url('assets_front/img/hero-covert.png')}}');" id="bg-hero"></div> --}}
        {{-- <iframe class="iframe-hide" style="border:none;" width="100%" height="100%" id="iframe360" allowvr="yes" frameborder="0" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen></iframe> --}}
    </div>
</div>


@push('scripts')
    <script>

        window.addEventListener('scroll', function(e) {
            var regelmnt = document.getElementById("scrollable-4");
            var regImgelmnt = document.getElementById("bg-chales-hero");

            var x = window.scrollY;
            var distanceFromTop = regelmnt.getBoundingClientRect().top; 
            var distanceOnBottom = (screen.height/2) - distanceFromTop;

            if( distanceOnBottom > 0 ){
                distanceOnBottom = distanceOnBottom/2
                if(distanceOnBottom > (regImgelmnt.width - window.innerWidth)){
                    distanceOnBottom = regImgelmnt.width - window.innerWidth;
                }
                $("#bg-chales-hero").css("transform", "translateX(-"+(distanceOnBottom)+"px)")

            }else {
                $("#bg-chales-hero").css("transform", "translateX(-"+(0)+"px)")
            }


            /* var newelmnt = document.getElementById("scrollable");
            newelmnt.scrollLeft = x; */
            /* $("#bg-hero").animate({ "right": (x)+"px" }, 0) */


        });

    </script>
@endpush

