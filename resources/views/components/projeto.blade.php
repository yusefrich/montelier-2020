<div id="project" class="container-large mx-auto overflow-hidden">
    <div class="row">
        <div class="col-md-4">
            <div data-aos="fade-right" style="background-image: url('{{url('assets_front/img/project-title-bg.png')}}');"
                class="project-header">
                <h2 class="text-light">PROJETO</h2>
            </div>
        </div>
        <div data-aos="fade-down" class="col-md-8">
            <div data-aos="fade-left" class="user-content-holder d-md-flex">
                <div data-aos="fade-left" data-aos-delay="200" class="user-pic d-flex d-md-block">
                    <img class="img-fluid" src="{{url('assets_front/img/foto-arq.png')}}" alt="">
                    <h3 class="d-md-none pb-3">ARQUITETURA</h3>
                </div>
                <div data-aos="fade-left" data-aos-delay="400" class="user-content-info">
                    <h3 class="d-none d-md-block">ARQUITETURA</h3>
                    <p>À frente da LMaia Arquitetos, Léo Maia desconhece barreiras em relação à sua produção. Com
                        uma
                        arquitetura que elevou o status de escritório em escala regional para uma esfera nacional,
                        tornou-se um dos profissionais mais reconhecidos da sua geração, tendo atualmente, assinaturas
                        nas
                        mais importantes cidades brasileiras.</p>
                    <p>Pesquisador e viajante nato, Léo Maia comanda uma equipe
                        de
                        jovens profissionais que posicionaram um estilo próprio na arquitetura proposta pela companhia,
                        passando a assinar alguns dos empreendimentos mais exclusivos do mercado, tais como
                        corporativos,
                        residenciais e hoteleiros.</p>
                </div>
            </div>
            <div data-aos="fade-left" class="user-content-holder d-md-flex">
                <div data-aos="fade-left" data-aos-delay="200" class="user-pic d-flex d-md-block">
                    <img class="img-fluid" src="{{url('assets_front/img/foto-urb.png')}}" alt="">
                    <h3 class="d-md-none pb-3">URBANISMO</h3>
                </div>
                <div data-aos="fade-left" data-aos-delay="400" class="user-content-info">
                    <h3 class="d-none d-md-block">URBANISMO</h3>
                    <p>Expedito Arruda foi professor por vários anos do curso de Arquitetura da universidade Federal da
                        Paraíba, fez parte do conselho do CREA e participou da fundação do IAB. Expedito é um
                        apaixonado pela arquitetura e pelo desenho, possui muitas obras icônicas no estado da Paraíba
                        e no seu escritório formou e influenciou um vasto número de brilhantes arquitetos que hoje
                        atuam no estado.</p>
                    <p>A influência moderna e pós-moderna no seu trabalho pode ser constatada nos
                        inúmeros projetos executados pelas maiores construtoras da Paraíba e de Pernambuco.</p>
                </div>
            </div>
            <div data-aos="fade-left" class="user-content-holder d-md-flex">
                <div data-aos="fade-left" data-aos-delay="200" class="user-pic d-flex d-md-block">
                    <img class="img-fluid" src="{{url('assets_front/img/foto-pai.png')}}" alt="">
                    <h3 class="d-md-none pb-3">PAISAGISMO</h3>
                </div>
                <div data-aos="fade-left" data-aos-delay="400" class="user-content-info">
                    <h3 class="d-none d-md-block">PAISAGISMO</h3>
                    <p>A arquiteta, urbanista e paisagista Patrícia Lago graduou-se no curso de Arquitetura e Urbanismo
                        pela Universidade Federal de Pernambuco - UFPE no ano de 1994. Tem passagem profissional focada
                        no Nordeste, residindo nos estados de Pernambuco, Rio Grande do Norte e Paraíba. Patrícia vem
                        sendo reconhecida principalmente por sua atuação na área de arquitetura paisagística,
                        prestando serviço às maiores construtoras da região.</p>
                </div>
            </div>
        </div>
    </div>
</div>