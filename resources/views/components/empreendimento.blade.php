<div id="emp" class="banner-holder position-relative overflow-hidden pt-1">
    <div data-aos="fade-left" class="bg-right d-none d-md-block" style="background-image: url('{{url('storage/home/'.$home2[0]['background'])}}');"></div>
    <div class="container-large mx-auto text-banner px-mdb-16">
        <h1 data-aos="fade-right" data-aos-delay="300" class="text-dark text-uppercase mb-32">{{$home2[0]['title']}}</h1>
        <img data-aos="fade-right" data-aos-delay="400" class="img-fluid img-name mb-32" src="{{url('storage/home/'.$home2[0]['img'])}}" alt="">
        <p data-aos="fade-right" data-aos-delay="500" class="text-dark mb-32">{{$home2[0]['subtitle']}}</p>
    </div>
</div>
<div data-aos="fade-up">
    @include('components._lib_static', ['type' => 'empreendimento', 'photos' => $home2[0]['photos']])
</div>
