<div class="container-large promo-holder mx-auto ">
    <div id="sceneH">
        <div data-relative-input="true" data-depth="0">
            <div data-aos="fade-up" class="chales-promo position-relative">
                <div class="position-relative">
                    <img style="max-height: 700px; object-fit: cover" class="main-img"
                        src="{{url('assets_front/img/chales-promo-1-n.jpg')}}" alt="">
                    <div class="main-img-blur"></div>
                </div>
                <div style="z-index: 10" class=" el-paral">
                    <div data-aos="fade-down" data-aos-delay="400" class="mont-box mont-box-right">
                        <img class="box-img-logo" src="{{url('assets_front/img/chales-promo-logo.png')}}" alt="">
                        <div class="chales-spacing-box">
                            <h3 class="text-uppercase">Um chalé completo PARA SE VIVER</h3>
                            <p >Chalés prontos para viver o verdadeiro clima da serra, com um cenário sem igual e um
                                aconchego sem medida.</p>
                            <a  href="https://chales.condominiomontelier.com.br" target="_blank"
                                class="btn btn-dark">VISITE O SITE <img style="position: relative; top: -2px"
                                    class="pl-1" src="{{url('assets_front/img/arrow.svg')}}" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class=" el-paral">
                    <img style="max-height: 250px; object-fit: cover" class="img-rb"
                        src="{{url('assets_front/img/chales-promo-3.jpg')}}" alt="">
                </div>
                <div class=" el-paral">
                    <img style="max-height: 340px; object-fit: cover" class="img-b"
                        src="{{url('assets_front/img/chales-promo-2.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>
     <div id="lsceneH">
        <div data-relative-input="true" data-depth="0">
            <div data-aos="fade-up" id="lofts" class="chales-promo position-relative text-md-right">

                <div class="position-relative">
                    <img style="max-height: 700px; object-fit: cover" class="main-img"
                        src="{{url('assets_front/img/g1/1.jpg')}}" alt="">
                    <div class="main-img-blur-right"></div>
                </div>
                <div style="z-index: 10;" class="el-paral">

                        <div data-aos="fade-down" data-aos-delay="400" class="mont-box mont-box-left text-left">
                            <img class="box-img-logo" id="lofts-logo"
                                src="{{url('assets_front/img/lofts_logo.png')}}" alt="">
                            <div class="chales-spacing-box">
                                <h3 class="text-uppercase">UM CONVITE PARA CONTEMPLAR A VIDA</h3>
                                <p >Os Lofts Montelier te inspiram a fugir da rotina, apreciar a natureza e curtir
                                    momentos
                                    únicos em
                                    meio ao brejo paraibano.</p>
                                <a href="https://lofts.condominiomontelier.com.br/" target="_blank"
                                    class="btn btn-dark">VISITE
                                    O SITE
                                    <img style="position: relative; top: -2px" class="pl-1"
                                        src="{{url('assets_front/img/arrow.svg')}}" alt=""></a>
                            </div>
                        </div>
                </div>
                <div class="el-paral">

                        <img style="max-height: 250px; object-fit: cover" class="img-lb"
                            src="{{url('assets_front/img/g1/2.jpg')}}" alt="">
                </div>
                <div class="el-paral">

                        <img style="max-height: 340px; object-fit: cover" class="img-b-2"
                            src="{{url('assets_front/img/g1/3.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>

</div>
@push('scripts')
<script>
    var vari = [];
    var p_el = document.getElementsByClassName("el-paral");
    var p_i;
    for (p_i = 0; p_i < p_el.length; p_i++) {
        vari.push(Math.floor(Math.random() * 50))
    }
    updateParallax();
    window.addEventListener('scroll', function(e) {
        updateParallax();
    });

    function updateParallax(){
        if(( window.innerWidth >= 765 )){
            var p_el = document.getElementsByClassName("el-paral");
            var p_i;
            for (p_i = 0; p_i < p_el.length; p_i++) {
                var x = window.innerHeight;
                var nx = window.innerHeight * -1;
                var distanceFromTop = p_el[p_i].getBoundingClientRect().top - (x/2);
                if(distanceFromTop >= x)
                    distanceFromTop = x;
                if(distanceFromTop <= nx)
                    distanceFromTop = nx;


                p_el[p_i].style.transform = "translateY(-"+((distanceFromTop /7) + vari[p_i])+"px)";
            }
            return;

        }

    }


</script>
@endpush
