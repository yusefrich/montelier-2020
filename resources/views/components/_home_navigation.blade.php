<ul class="navbar-nav text-center text-md-left mx-auto mx-md-0 d-none d-md-flex">
    <li class="nav-item active">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'exp360')" href="#">O Montelier</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'hero-regiao')" href="#">A região </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'hero-loteamento')" href="#">Lotes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'hero-chales')" href="#">Chalés</a>
    </li>
   <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'lsceneH')" href="#">Lofts</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenuHome('#navbarNav', 'contact')" href="#">Contato</a>
    </li>
</ul>
<ul class="navbar-nav text-center text-md-left mx-auto mx-md-0 d-md-none">
    <li class="nav-item active">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#exp360">O Montelier</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#hero-regiao">A região </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#hero-loteamento">Lotes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#hero-chales">Chalés</a>{{--  e Lofts --}}
    </li>
    {{-- <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#lsceneH">Lofts</a>
    </li> --}}
    <li class="nav-item">
        <a class="nav-link text-light text-uppercase" onclick="collapseMenu('#navbarNav')" href="#contact">Contato</a>
    </li>
</ul>
