<div class="bg-dark-green">
    <div class="container-large mx-auto list-spacing " id="regiao-list">
        <div data-aos="fade-right" class="mont-collapse" id="accordion-2">
            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left" data-toggle="collapse"
                        data-target="#c-bananeiras" aria-expanded="true" aria-controls="c-bananeiras">
                        ACESSO POR BANANEIRAS
                    </button>
                </div>
                <div  class="col-md-7 list-data">
                    <div style="right: 0" id="c-bananeiras" class="animate__animated animate__fadeIn collapse show  text-right mr-md-3" aria-labelledby="h-bananeiras"
                        data-parent="#accordion-2">
                        <img data-aos="fade-right" data-aos-delay="400" data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/1-Bananeiras-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/1-Bananeiras-min.jpg')}}" alt="">
                        <div class="mont-box text-left">
                                <img data-sizes="auto" class="img-fluid img-map lazyload" data-src="{{asset('assets_front/img/reg-lib-map.png')}}" data-srcset="{{asset('assets_front/img/reg-lib-map.png')}}" alt="">
                                <p>Em Bananeiras, que fica a pouco tempo de João Pessoa, Campina Grande e Natal, você pode aproveitar a vista imperdível da Antiga Estação Ferroviária, além de passar pelo principal ponto turístico da cidade, o Túnel da Serra da Viração. Após o túnel, você se depara com o Riacho do Damião, que deságua na famosa Cachoeira do Roncador.
                                </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse"
                        data-target="#c-borborema" aria-expanded="true" aria-controls="c-borborema" >
                        ACESSO POR BORBOREMA
                    </button>
                </div>
                <div  class="col-md-7 list-data">
                    <div style="right: 0" id="c-borborema" class="animate__animated animate__fadeIn collapse  text-right mr-md-3" aria-labelledby="h-borborema"
                        data-parent="#accordion-2">
                        <img data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/2-Borborema-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/2-Borborema-min.jpg')}}" alt="">
                        <div class="mont-box text-left">
                            <img data-sizes="auto" class="img-fluid img-map lazyload" data-src="{{asset('assets_front/img/reg-lib-map-2.png')}}" data-srcset="{{asset('assets_front/img/reg-lib-map-2.png')}}" alt="">
                            <p>Ao chegar ao Montelier por Borborema, você aprecia um belíssimo percurso. A cidade, que traz consigo casas de época, é cercada por riachos e lindos lagos. Quem passa por lá não pode perder a oportunidade de conhecer os engenhos da cidade e a barragem local.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse"
                        data-target="#c-solanea" aria-expanded="true" aria-controls="c-solanea" >
                        ACESSO POR SOLÂNEA
                    </button>
                </div>
                <div  class="col-md-7 list-data">
                    <div style="right: 0" id="c-solanea" class="animate__animated animate__fadeIn collapse  text-right mr-md-3" aria-labelledby="h-solanea"
                        data-parent="#accordion-2">
                        <img data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/3-Solanea-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/3-Solanea-min.jpg')}}" alt="">
                        <div class="mont-box text-left">
                            <img data-sizes="auto" class="img-fluid img-map lazyload" data-src="{{asset('assets_front/img/reg-lib-map-3.png')}}" data-srcset="{{asset('assets_front/img/reg-lib-map-3.png')}}" alt="">
                            <p>Já por Solânea, outra cidade de acesso ao condomínio, o caminho é urbanizado e com diversas opções para quem deseja realizar compras ou contratar serviços.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse"
                        data-target="#c-Cultura" aria-expanded="true" aria-controls="c-Cultura" >
                        Cultura
                    </button>
                </div>
                <div  class="col-md-7 list-data">
                    <div style="right: 0" id="c-Cultura" class="animate__animated animate__fadeIn collapse  text-right mr-md-3" aria-labelledby="h-Cultura"
                        data-parent="#accordion-2">
                        <img data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/4-Cultura-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/4-Cultura-min.jpg')}}" alt="">
                        <div class="mont-box  text-left">
                            {{-- <img class="img-fluid img-map" src="{{asset('assets_front/img/reg-lib-map-3.png')}}" alt=""> --}}
                            <h4>EXPERIÊNCIA ÚNICA DENTRO E FORA</h4>
                            <p class="pt-0">O condomínio Montelier está situado em uma área repleta de experiências turísticas, culturais e gastronômicas, como o tradicional São João de Bananeiras, a rota Caminhos do Frio, além dos ótimos restaurantes e diversas feiras livres.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse"
                        data-target="#c-Aventura" aria-expanded="true" aria-controls="c-Aventura" >
                        Aventura
                    </button>
                </div>
                <div  class="col-md-7 list-data">
                    <div style="right: 0" id="c-Aventura" class="animate__animated animate__fadeIn collapse  text-right mr-md-3" aria-labelledby="h-Aventura"
                        data-parent="#accordion-2">
                        <img data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/5-Aventura-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/5-Aventura-min.jpg')}}" alt="">
                        <div class="mont-box  text-left">
                            {{-- <img class="img-fluid img-map" src="{{asset('assets_front/img/reg-lib-map-3.png')}}" alt=""> --}}
                            <h4>CONHEÇA TRILHAS, MOTOCROSS</h4>
                            <p class="pt-0">Para quem ama aventura, a região conta com trilhas de off-road pelas estradas de terra que passam por paisagens belíssimas, como o Lajedo Preto. Os passeios de bike, quadriciclo, motocross e cavalgadas são uma atração à parte.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div data-aos="fade-right" data-aos-delay="100" class="col-md-5">
                    <button class="btn c-btn-list btn-block btn-underline text-left collapsed" data-toggle="collapse"
                        data-target="#c-Natureza" aria-expanded="true" aria-controls="c-Natureza" onclick="preventCollapse(this)">
                        Natureza
                    </button>
                </div>
                <div class="col-md-7 list-data">
                    <div style="right: 0" id="c-Natureza" class="animate__animated animate__fadeIn collapse  text-right mr-md-3" aria-labelledby="h-Natureza"
                        data-parent="#accordion-2">
                        <img data-sizes="auto" class="img-fluid img-list-bg lazyload" data-src="{{asset('assets_front/img/g2/6-Natureza-min.jpg')}}" data-srcset="{{asset('assets_front/img/g2/6-Natureza-min.jpg')}}" alt="">
                        <div class="mont-box  text-left">
                            {{-- <img class="img-fluid img-map" src="{{asset('assets_front/img/reg-lib-map-3.png')}}" alt=""> --}}
                            <h4>VISITE A CACHOEIRA DO RONCADOR E MAIS</h4>
                            <p class="pt-0">As águas frias das cachoeiras e as escaladas na Pedra da Boca tornam qualquer passeio inesquecível. E você certamente vai se encantar com a história dos engenhos e casarões locais, e curtir passeios em família pelo Cruzeiro de Roma.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

