<div class="container-large mx-auto info-map-container position-relative ">
    <div class="border-left-line">

        <div data-aos="fade-right" class="mont-box overflow-visible">
            <h4  class="pb-3">196 lotes</h4>
            <p >Terreno padrão de 352 m², sendo 16m de largura frontal e 22m
                de profundidade, e outros terrenos com formatos
                e tamanhos diversos para você planejar como quiser.</p>
            <div  class="info-map d-none d-md-flex">
                <img style="object-fit: contain;" src="{{url('assets_front/img/map-dot.png')}}" alt="">
                <p style="max-width: 186px;">Passe o mouse sobre os pontos laranjas para visualizar suas legendas</p>
            </div>
        </div>
        <div class="info-map d-flex d-md-none mt-32 ml-24">
            <p style="max-width: 186px;">Toque no mapa para ampliar</p>
        </div>
    </div>

    {{-- <img src="{{url('assets_front/img/map-full.png')}}" class="map-full-detail d-none d-md-block"
    alt="loteamento-montelier-mapa"> --}}
    <div data-aos="fade-up" data-aos-delay="500" class="d-none d-md-block map-full position-relative">
        <img src="{{url('assets_front/img/map-full.png')}}" class="map-full-detail " alt="loteamento-montelier-mapa">
        <div style="left: 65.96%; top: 3.25%;" class="map-point-point text-center position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn ">haras</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 58.09%; top: 12.27%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">quadra poliesportiva</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 62.39%; top: 16.13%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">quadra de tênis</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 57.96%; top: 17.61%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">academia</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 66.33%; top: 20.98%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">camping</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 59.63%; top: 22.45%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">salão de festas</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 63.13%; top: 24.23%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">piscina</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 68.49%; top: 24.60%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">pista de cooper</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 56.18%; top: 24.97%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">espaço kids</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 57.76%; top: 25.90%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">casa na árvore</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 51.50%; top: 26.99%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">guarita</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 42.40%; top: 27.36%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">entrada de serviço</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 85.28%; top: 48.71%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">trilha para cavalos</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 47.93%; top: 50.80%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">lotes</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 33.29%; top: 56.13%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">espaço gourmet</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 81.72%; top: 62.02%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">lofts</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 85.28%; top: 79.75%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">chalés</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
        <div style="left: 74.21%; top: 84.23%;" class="map-point-point position-absolute">
            <p class="text-uppercase animate__animated animate__fadeIn">redário</p>
            {{-- <div class="vl">
                <div class="vll"></div>
            </div> --}}
        </div>
    </div>
    {{-- <div data-aos="fade-down" class="zoom">
        <img src="{{url('assets_front/img/map-full-mobile.png')}}" class="map-full-detail "
    alt="loteamento-montelier-mapa">
</div> --}}
</div>
<div class="overflow-hidden">

    <div style="width: 160%; transform: translateX(-30%);" data-aos="fade-down" class="d-md-none map-full "
        data-toggle="modal" data-target="#mapZoom">
        <img src="{{url('assets_front/img/map-full-mobile.png')}}" class="map-full-detail "
            alt="loteamento-montelier-mapa">
    </div>
</div>

@push('modais')
<div class="modal fade pr-0" id="mapZoom" tabindex="-1" role="dialog" aria-labelledby="mapZoomLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-full">
        <div class="modal-content bg-transparent">
            <div class="modal-body px-0">
                <div class="d-flex justify-content-end">
                    <button style="z-index: 10" type="button" class="btn btn-light btn-round p-3 mr-4"
                        data-dismiss="modal" aria-label="Close">
                        <i class="icon icon-cross"></i>
                    </button>
                </div>
                <div style="width: 100vw" id="carousel_img_obras"
                    class="carousel position-absolute slide vertical-center" data-ride="carousel">
                    <div class="carousel-inner overflow-visible">
                        <div class="carousel-item px-200 img-carousel-lib active">
                            <div data-aos="fade-down" class="zoom">
                                <img src="{{url('assets_front/img/map-full-mobile.png')}}" class="map-full-detail "
                                    alt="loteamento-montelier-mapa">
                            </div>
                            {{-- <div class="carousel-caption d-none d-md-block img-desc">
                                <h4 data-bind="html: title" class="text-light lib-desc-offset text-uppercase"></h4>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer border-0">
            </div> --}}
        </div>
    </div>
</div>
@endpush

@push('scripts')
<script>

</script>
@endpush