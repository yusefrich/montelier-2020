<div id="downloads" class="bg-dark-green">
    <div class="container-large mx-auto ">
        <div class="row pt-100 pb-100 pt-mdb-80 pb-mdb-80 mx-0">
            <div  class="col-md-6">
                <h1 style="max-width: 546px;" data-aos="fade-right" class="text-light text-uppercase mb-4 mb-md-2 d-none d-md-block">Obtenha o nosso book DIGITAL</h1>
                <h1 style="max-width: 300px;" data-aos="fade-right" class="text-light text-uppercase mb-4 mb-md-2 d-md-none">Obtenha o nosso book DIGITAL</h1>
            </div>
            <div class="col-md-6 overflow-hidden">
                {!! Form::open(['class' => 'text-start montelier-form', 'id' => 'form-contact']) !!}
                    <div class="form-group" data-aos="fade-left">
                        {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'SEU NOME',
                        'id'=> 'input-name', 'required']) !!}
                    </div>
                    <div class="form-group  " data-aos="fade-left">
                        {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                        {!! Form::text('email', null, ['class' => 'form-control','placeholder' =>
                        'EMAIL', 'id'=> 'input-email', 'required']) !!}
                    </div>
                    <div class="form-group " data-aos="fade-left">
                        {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00) 00000-0000','placeholder' =>
                        'TELEFONE', 'id'=>
                        'input-phone', 'required']) !!}
                    </div>
                    {{-- <p data-aos="fade-left" data-aos-delay="300" class="text-light mt-32">O que você deseja baixar?</p> --}}
                    <div  style="opacity:0; position:absolute; left:9999px;">
                        <div  class="custom-control custom-checkbox ">
                            <input hidden type="checkbox"  name="book" class="custom-control-input" id="bookcheck">
                            <label class="custom-control-label text-light text-uppercase" for="bookcheck">
                                Book digital</label>
                        </div>
                    </div>
                    <button data-aos="fade-left" data-aos-delay="700" class="btn btn-outline-light btn-spacing mb-32 mt-32"  type="submit">Obter <i class="icon  ml-16"></i></button>
                    <p><small data-aos="fade-left" data-aos-delay="900" class="text-light text-uppercase">Ao obter nossos materiais, você concorda com nossa Política de Privacidade</small></p>
                    <div class="alert alert-success ok" role="alert">
                      Dados enviados com sucessso. Acesse seu email para acessar o centeúdo!
                    </div>
                </form>
            </div>
        </div>
        {{-- <hr class="bg-light m-0"> --}}

    </div>
</div>

@push('scripts')
<script>

    $(".ok").hide();
    $('#form-contact').submit(function() {

        var formData = {
            'name' : $('input[name=name]').val(),
            'email' : $('input[name=email]').val(),
            'phone' : $('input[name=phone]').val(),
            'book' : true,
            'table' : $('input[name=table]').is(":checked"),
            '_token': '{{csrf_token()}}'
        };

        $.ajax({
            type        : 'POST',
            url         : '{{route('contact')}}',
            data        : formData,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {
            $('.ok').show();
        });
        event.preventDefault();
    });
</script>
<script>



</script>

@endpush
