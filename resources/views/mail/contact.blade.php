@component('mail::message')
<h4>Olá, {{$data->name}}</h4><br><br>

<p>Obrigado pelo seu interesse no Condomínio Montélier.</p>
<p>Clique <a href="https://condominiomontelier.com.br/BookMontelier.pdf" target="_blank">aqui</a> para acessar nosso Book Digital. </p>
<br>
<br>
<p>Condominio Montelier</p>
@endcomponent
