<?php

use App\Models\Support;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lc_pc')->nullable();
            $table->string('lc_mobile')->nullable();
            $table->string('lc_url')->nullable();
            $table->string('file1')->nullable();
            $table->string('file2')->nullable();
            $table->string('email')->nullable();
            $table->string('co_txt')->nullable();
            $table->string('co_bt_txt')->nullable();
            $table->string('co_bt_ico')->nullable();
            $table->string('co_bt_url')->nullable();
            $table->string('co_1_ico')->nullable();
            $table->string('co_1_txt')->nullable();
            $table->string('co_2_ico')->nullable();
            $table->string('co_2_txt')->nullable();
            $table->string('co_3_ico')->nullable();
            $table->string('co_3_txt')->nullable();
            $table->string('fc_1_ico')->nullable();
            $table->string('fc_1_txt')->nullable();
            $table->string('fc_1_url')->nullable();
            $table->string('fc_2_ico')->nullable();
            $table->string('fc_2_txt')->nullable();
            $table->string('fc_2_url')->nullable();
            $table->string('fc_3_ico')->nullable();
            $table->string('fc_3_txt')->nullable();
            $table->string('fc_3_url')->nullable();
            $table->string('fc_4_ico')->nullable();
            $table->string('fc_4_txt')->nullable();
            $table->string('fc_4_url')->nullable();
            $table->text('tl_txt')->nullable();
            $table->text('pc_txt')->nullable();
            $table->timestamps();
        });

        Support::create([]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support');
    }
}
