<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $table = 'support';

    protected $fillable = [
        'lc_pc',
        'lc_mobile',
        'lc_url',
        'file1',
        'file2',
        'email',
        'co_txt',
        'co_bt_txt',
        'co_bt_ico',
        'co_bt_url',
        'co_1_ico',
        'co_1_txt',
        'co_2_ico',
        'co_2_txt',
        'co_3_ico',
        'co_3_txt',
        'fc_1_ico',
        'fc_1_txt',
        'fc_1_url',
        'fc_2_ico',
        'fc_2_txt',
        'fc_2_url',
        'fc_3_ico',
        'fc_3_txt',
        'fc_3_url',
        'fc_4_ico',
        'fc_4_txt',
        'fc_4_url',
        'tl_txt',
        'pc_txt',
    ];
}
