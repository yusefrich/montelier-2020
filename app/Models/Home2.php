<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home2 extends Model
{
    protected $table = 'home2';

    protected $fillable = [
        'title', 'img', 'subtitle', 'background'
    ];

    public function photos()
    {
        return $this->hasMany(HomePhoto::class, 'home_id');
    }
}
