<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{    
    use Queueable, SerializesModels;

    public $data;
    public $support;

    public function __construct($data, $support)
    {
        $this->data = $data;
        $this->support = $support;
    }
    
    public function build()
    {
        $support = $this->support;

        $build = $this->subject('Contato '.config('app.name'))
                    ->markdown('mail.contact');
        /* if ($this->data->book && $support->file1) {
            $build->attach(url('storage/support/'.$support->file1));
        }
        if ($this->data->table && $support->file2) {
            $build->attach(url('storage/support/'.$support->file2));
        } */

        return $build;
    }
}
